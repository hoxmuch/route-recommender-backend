import matplotlib.pyplot as plt

from math import log, exp
import time

import requests
from pymongo import MongoClient, ASCENDING
from bson.objectid import ObjectId

import numpy as np
from python_tsp.exact import solve_tsp_dynamic_programming

client = MongoClient('localhost', 27017)
db = client['route-recommender']

categories_url = 'https://kudago.com/public-api/v1.4/place-categories/?fields=slug,name'
places_url = 'https://kudago.com/public-api/v1.4/places/'
metro_url = 'https://api.hh.ru/metro/1'
distance_matrix_url = 'https://api.routing.yandex.net/v2/distancematrix'
api_key = '35377ec8-55a4-4b25-9e66-dd50376ce389'

def load_categories():
    r = requests.get(categories_url)
    categories = r.json()
    for category in categories:
        db.categories.insert_one(category)

def load_places():
    r = requests.get(places_url)
    places = r.json()
    i = 1
    while 'next' in places:
        for place in places['results']:
            print(i)
            detail_place_request = requests.get(places_url + str(place['id']))
            detail_place = detail_place_request.json()
            detail_place.pop('id', None)
            db.places.insert_one(detail_place)
            i += 1

        if places['next'] != 'null':
            r = requests.get(places['next'])
            places = r.json()

def load_metro():
    r = requests.get(metro_url)
    metros = r.json()
    lines = metros['lines']
    names = []
    for line in lines:
        stations = line['stations']
        for station in stations:
            if db.stations.find_one({'name': station['name']}) is None:
                db.stations.insert_one({
                    'lat': station['lat'],
                    'lng': station['lng'],
                    'name': station['name']
                })
                names.append(station['name'])

    default_duration = dict()
    for name in names:
        default_duration[name] = 0

    for station in db.stations.find():
        db.stations.update_one(
            {'_id': station['_id']},
            {'$set': {'duration': default_duration}}
        )

def update_categories():
    tags = dict()
    for place in db.places.find():
        if 'tags' in place and place['tags'] is not []:
            for tag in place['tags']:
                upper_tag = tag[0].upper() + tag[1:]
                tags[upper_tag] = 0

    count = 0
    for tag in tags.keys():
        if db.categories.find_one({'name': tag}) is None:
            print(tag)
            count += 1
            db.categories.insert_one({'name': tag})
    print(len(tags.keys()))
    print(count)

def update_vectors():
    categories_vector = dict()
    slug_dict = dict()
    for category in db.categories.find():
        categories_vector[category['name']] = 0
        if 'slug' in category:
            slug_dict[category['slug']] = category['name']

    for place in db.places.find():
        for category in categories_vector:
            categories_vector[category] = 0
        if 'categories' in place:
            for category in place['categories']:
                if category in slug_dict and slug_dict[category] in categories_vector:
                    categories_vector[slug_dict[category]] = 1
        if 'tags' in place:
            for tag in place['tags']:
                upper_tag = tag[0].upper() + tag[1:]
                if upper_tag in categories_vector:
                    categories_vector[upper_tag] = 1
        db.places.update_one(
            {'_id': place['_id']},
            {'$set': {'vector': categories_vector}}
        )

def update_user_vectors():
    categories_vector = dict()
    slug_dict = dict()
    for category in db.categories.find():
        categories_vector[category['name']] = 0
        if 'slug' in category:
            slug_dict[category['slug']] = category['name']

    for user in db.users.find():
        vector = user['vector']
        new_vector = vector.copy()
        for category in vector:
            if category not in categories_vector:
                del new_vector[category]
        db.users.update_one(
            {'_id': user['_id']},
            {'$set': {'vector': new_vector}}
        )

def calculate_categories_count_histogram():
    categories = dict()
    slug_dict = dict()
    for category in db.categories.find():
        categories[category['name']] = 0
        if 'slug' in category:
            slug_dict[category['slug']] = category['name']

    for place in db.places.find():
        if 'categories' in place:
            for category in place['categories']:
                if slug_dict[category] in categories:
                    categories[slug_dict[category]] += 1
                else:
                    categories[slug_dict[category]] = 1
        if 'tags' in place:
            for tag in place['tags']:
                upper_tag = tag[0].upper() + tag[1:]
                if upper_tag in categories:
                    categories[upper_tag] += 1
                else:
                    categories[upper_tag] = 1
    less5 = 0
    for category in categories:
        if categories[category] <= 5:
            less5 += 1
        print(category + " " + str(categories[category]))
    print(less5)

def print_all_categories():
    count = 0
    for category in db.categories.find():
        count += 1
        print(category['name'])
    print(count)

def delete_useless_places():
    db.places.delete_many({"location": {"$ne": "msk"}})
    db.places.delete_many({"subway": ""})
    db.places.delete_many({"categories": "restaurants"})
    db.places.delete_many({"categories": "bar"})
    db.places.delete_many({"categories": "questroom"})
    db.places.delete_many({"categories": "education-centers"})
    db.places.delete_many({"categories": "dance-studio"})
    db.places.delete_many({"categories": "hostels"})
    db.places.delete_many({"categories": "inn"})
    db.places.delete_many({"categories": "salons"})
    db.places.delete_many({"categories": "shops"})

def delete_less_categories():
    categories = dict()
    slug_dict = dict()
    for category in db.categories.find():
        categories[category['name']] = 0
        if 'slug' in category:
            slug_dict[category['slug']] = category['name']

    for place in db.places.find():
        if 'categories' in place:
            for category in place['categories']:
                if slug_dict[category] in categories:
                    categories[slug_dict[category]] += 1
                else:
                    categories[slug_dict[category]] = 1
        if 'tags' in place:
            for tag in place['tags']:
                upper_tag = tag[0].upper() + tag[1:]
                if upper_tag in categories:
                    categories[upper_tag] += 1
                else:
                    categories[upper_tag] = 1

    for category in categories:
        if categories[category] == 0:
            db.categories.delete_one({"name": category})
            db.places.delete_many({"tags": category.lower()})
            if category in slug_dict:
                db.places.delete_many({"categories": slug_dict[category]})

def create_weights():
    categories = dict()
    slug_dict = dict()
    for category in db.categories.find():
        categories[category['name']] = 0
        if 'slug' in category:
            slug_dict[category['slug']] = category['name']

    for place in db.places.find():
        if 'categories' in place:
            for category in place['categories']:
                if category in slug_dict and slug_dict[category] in categories:
                    categories[slug_dict[category]] += 1
        if 'tags' in place:
            for tag in place['tags']:
                upper_tag = tag[0].upper() + tag[1:]
                if upper_tag in categories:
                    categories[upper_tag] += 1

    for category in categories:
        print(category)
        print(categories[category])
        common_log_category = 1 + log(categories[category])
        common_weight = 1 / ((1 + exp(-common_log_category))**4)
        db.common_weights.insert_one({
            'category': category,
            'weight': common_weight
        })

        uncommon_weight = 1 / (1 + log(categories[category]))
        db.uncommon_weights.insert_one({
            'category': category,
            'weight': uncommon_weight
        })

def test_tsp():
    distance_matrix = np.array([
        [0, 1.5, 1.5, 7.3],
        [0, 0, 7.3, 1.5],
        [0, 7.3, 0, 1.5],
        [0, 1.5, 1.5, 0]
    ])
    permutation, distance = solve_tsp_dynamic_programming(distance_matrix)
    print(permutation)
    print(distance)

def calculate_distance_matrix():
    same_stations_time = 5 * 60
    i = 0
    for orig_place in db.places.find({'distance': {'$exists': False}}):
        distance = dict()
        orig_id = orig_place['_id']
        orig_subways = orig_place['subway'].split(', ')
        for dest_place in db.places.find():
            dest_id = dest_place['_id']
            dest_subways = dest_place['subway'].split(', ')
            if dest_id == orig_id:
                duration = 0
            elif list(set(orig_subways) & set(dest_subways)):
                duration = same_stations_time
            else:
                min_duration = 9999999999999
                for orig_subway in orig_subways:
                    orig_station = db.stations.find_one({'name': orig_subway})
                    if orig_station is not None:
                        orig_station_duration = orig_station['duration']
                        for dest_subway in dest_subways:
                            if dest_subway in orig_station_duration and orig_station_duration[dest_subway] < min_duration:
                                min_duration = orig_station_duration[dest_subway]
                if min_duration == 9999999999999:
                    min_duration = 3600
                duration = min_duration
            distance[str(dest_id)] = duration

        i += 1
        print(i)
        db.places.update_one(
            {'_id': orig_id},
            {'$set': {'distance': distance}}
        )

def load_metro_distance_matrix():
    rc = 0
    rps = 50
    for orig_station in db.stations.find():
        destinations = []
        orig_lat = orig_station['lat']
        orig_lon = orig_station['lng']
        orig_id = orig_station['_id']
        distance = dict()

        for dest_station in db.stations.find():
            destinations.append(dest_station)
            if len(destinations) == 100:
                url = distance_matrix_url + '?' + 'apikey=' + api_key + '&mode=transit&origins=' + str(
                    orig_lat) + ',' + str(orig_lon) + '&destinations='
                for i in range(len(destinations)):
                    destination = destinations[i]
                    dest_lat = destination['lat']
                    dest_lon = destination['lng']
                    url += str(dest_lat) + ',' + str(dest_lon)
                    if i != len(destinations) - 1:
                        url += '|'

                r = requests.get(url)
                if r.status_code != 200:
                    print(r.status_code)
                    print(r.json())
                matrix = r.json()
                for row in matrix['rows']:
                    elem_i = 0
                    for elem in row['elements']:
                        if elem['status'] != 'OK':
                            duration = 0
                            print(destinations[elem_i]['name'])
                        else:
                            duration = elem['duration']['value']
                        dest_name = destinations[elem_i]['name']
                        distance[dest_name] = duration
                        elem_i += 1

                destinations = []
                rc = (rc + 1) % rps
                if rc == 0:
                    time.sleep(1)

        if destinations:
            url = distance_matrix_url + '?' + 'apikey=' + api_key + '&mode=transit&origins=' + str(
                orig_lat) + ',' + str(orig_lon) + '&destinations='
            for i in range(len(destinations)):
                destination = destinations[i]
                dest_lat = destination['lat']
                dest_lon = destination['lng']
                url += str(dest_lat) + ',' + str(dest_lon)
                if i != len(destinations) - 1:
                    url += '|'

            r = requests.get(url)
            matrix = r.json()
            for row in matrix['rows']:
                elem_i = 0
                for elem in row['elements']:
                    if elem['status'] != 'OK':
                        duration = 0
                        print(destinations[elem_i]['name'])
                    else:
                        duration = elem['duration']['value']
                    dest_name = destinations[elem_i]['name']
                    distance[dest_name] = duration
                    elem_i += 1

            destinations = []
            rc = (rc + 1) % rps
            if rc == 0:
                time.sleep(1)

        print(len(distance))
        db.stations.update_one(
            {'_id': orig_id},
            {'$set': {'duration': distance}}
        )

def make_research_data():
    count_t1 = 0
    count_t2 = 0
    feedbacks_dict = dict()

    common_mode_t1 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    black_list_mode_t1 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    soft_excluded_mode_t1 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    use_prev_history_mode_t1 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    use_common_weights_mode_t1 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    exclude_low_rang_mode_t1 = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    common_mode_t2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    black_list_mode_t2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    soft_excluded_mode_t2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    use_prev_history_mode_t2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    use_common_weights_mode_t2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    exclude_low_rang_mode_t2 = [0, 0, 0, 0, 0, 0, 0, 0, 0]

    for feedback in db.feedbacks.find():
        if feedback['feedback'] in feedbacks_dict:
            continue
        r = db.requests.find_one({'_id': ObjectId(feedback['rid'])})
        feedbacks_dict[feedback['feedback']] = 1

        if feedback['user_type'] == 'Я пользователь с небольшим опытом, знаю мало доостопримечательностей и интересных мест в Москве':
            count_t1 += 1
            if r['soft_excluded_categories'] == [] and r['hard_excluded_categories'] == [] and not r['use_prev_history'] and not r['use_common_weights'] and not r['exclude_low_rang_routes']:
                calc_feedback(common_mode_t1, feedback)
            if r['hard_excluded_categories']:
                calc_feedback(black_list_mode_t1, feedback)
            if r['soft_excluded_categories']:
                calc_feedback(soft_excluded_mode_t1, feedback)
            if r['use_prev_history']:
                calc_feedback(use_prev_history_mode_t1, feedback)
            if r['use_common_weights']:
                calc_feedback(use_common_weights_mode_t1, feedback)
            if r['exclude_low_rang_routes']:
                calc_feedback(exclude_low_rang_mode_t1, feedback)
        elif feedback['user_type'] == 'Я опытный пользователь, знающий много достопримечательностей и интересных мест в Москве':
            count_t2 += 1
            if r['soft_excluded_categories'] == [] and r['hard_excluded_categories'] == [] and not r['use_prev_history'] and not r['use_common_weights'] and not r['exclude_low_rang_routes']:
                calc_feedback(common_mode_t2, feedback)
            if r['hard_excluded_categories']:
                calc_feedback(black_list_mode_t2, feedback)
            if r['soft_excluded_categories']:
                calc_feedback(soft_excluded_mode_t2, feedback)
            if r['use_prev_history']:
                calc_feedback(use_prev_history_mode_t2, feedback)
            if r['use_common_weights']:
                calc_feedback(use_common_weights_mode_t2, feedback)
            if r['exclude_low_rang_routes']:
                calc_feedback(exclude_low_rang_mode_t2, feedback)
        else:
            return

    print("Неопытный пользователь:")
    print("Число отзывов: " + str(count_t1))
    print_feedback_stat(common_mode_t1, "Стандартный режим:", count_t1)
    print_feedback_stat(black_list_mode_t1, "Режим черного списка:",  count_t1)
    print_feedback_stat(soft_excluded_mode_t1, "Режим нежелательных категорий:",  count_t1)
    print_feedback_stat(use_prev_history_mode_t1, "Режим с использованием предыдущей истории:",  count_t1)
    print_feedback_stat(use_common_weights_mode_t1, "Режим с более частыми обычными местами:",  count_t1)
    print_feedback_stat(exclude_low_rang_mode_t1, "Режим с исключением маршрута с низким рангом:",  count_t1)
    print()

    print("Опытный пользователь:")
    print("Число отзывов: " + str(count_t2))
    print_feedback_stat(common_mode_t2, "Стандартный режим:", count_t2)
    print_feedback_stat(black_list_mode_t2, "Режим черного списка:", count_t2)
    print_feedback_stat(soft_excluded_mode_t2, "Режим нежелательных категорий:", count_t2)
    print_feedback_stat(use_prev_history_mode_t2, "Режим с использованием предыдущей истории:", count_t2)
    print_feedback_stat(use_common_weights_mode_t2, "Режим с более частыми обычными местами:", count_t2)
    print_feedback_stat(exclude_low_rang_mode_t2, "Режим с исключением маршрута с низким рангом:", count_t2)

def calc_feedback(arr, feedback):
    if feedback['overall'] == 'Положительное':
        arr[0] += 1
    elif feedback['overall'] == 'Нейтральное':
        arr[1] += 1
    else:
        arr[2] += 1

    if feedback['general'] == 'Соответствует ожиданиям, хорошо':
        arr[3] += 1
    elif feedback['general'] == 'Соответствует ожиданиям, плохо':
        arr[4] += 1
    elif feedback['general'] == 'Не соответствует ожиданиям, хорошо':
        arr[5] += 1
    else:
        arr[6] += 1

    if feedback['some_new']:
        arr[7] += 1
    if feedback['seen_before']:
        arr[8] += 1


def print_feedback_stat(arr, label, count):
    print()
    print(label)
    print(str(((arr[0] + arr[1] + arr[2]) / count) * 100) + "%")
    if arr[0] + arr[1] + arr[2] != 0:
        cur_count = arr[0] + arr[1] + arr[2]
    else:
        cur_count = 1
    print("Положительные: " + str(arr[0]) + ", " + str((arr[0] / cur_count) * 100) + "%")
    print("Нейтральные: " + str(arr[1]) + ", " + str((arr[1] / cur_count) * 100) + "%")
    print("Отрицательные: " + str(arr[2]) + ", " + str((arr[2] / cur_count) * 100) + "%")
    print("++: " + str(arr[3]) + ", " + str((arr[3] / cur_count) * 100) + "%")
    print("+-: " + str(arr[4]) + ", " + str((arr[4] / cur_count) * 100) + "%")
    print("-+: " + str(arr[5]) + ", " + str((arr[5] / cur_count) * 100) + "%")
    print("--: " + str(arr[6]) + ", " + str((arr[6] / cur_count) * 100) + "%")
    print("Новое: " + str(arr[7]) + ", " + str((arr[7] / cur_count) * 100) + "%")
    print("Уже видел: " + str(arr[8]) + ", " + str((arr[8] / cur_count) * 100) + "%")
    print()

if __name__ == '__main__':
    # load_categories()
    # load_places()
    # load_metro()
    # update_categories()
    # calculate_categories_count_histogram()
    # print_all_categories()
    # update_vectors()
    # update_user_vectors()
    # delete_useless_places()
    # delete_less_categories()
    # create_weights()
    # test_tsp()
    # calculate_distance_matrix()
    # load_metro_distance_matrix()
    make_research_data()
    pass

