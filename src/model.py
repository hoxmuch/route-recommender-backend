from src.repository import RouteRecommenderRepository
from src.token_manager import TokenManager

import numpy as np
from scipy.spatial import distance
from sklearn.preprocessing import normalize

from python_tsp.exact import solve_tsp_dynamic_programming

class RouteRecommenderModel:
    def __init__(self, repository: RouteRecommenderRepository, token_manager: TokenManager):
        self.__repo = repository
        self.__token_manager = token_manager

    def login(self, login, password):
        user_id = self.__repo.login(login, password)
        if user_id is not None:
            token = self.__token_manager.create_token(user_id)
            return token, user_id, None
        return None, None, "Wrong login or password"

    def register(self, login, password):
        user_id = self.__repo.register(login, password)
        if user_id is not None:
            token = self.__token_manager.create_token(user_id)
            return token, user_id, None
        return None, None, "User with this login already exists"

    def get_story(self, user_id):
        return self.__repo.get_story(user_id)

    def get_metro_stations(self):
        stations = self.__repo.get_metro_stations()
        station_names = []
        for station in stations:
            station_names.append(station['name'])
        return station_names

    def get_categories(self):
        categories = self.__repo.get_categories()
        categories_names = []
        for category in categories:
            categories_names.append(category['name'])
        return categories_names

    def safe_feedback(self, rid, feedback):
        self.__repo.safe_feedback(rid, feedback)

    def build_next_batch_route(self, rid):
        data = self.__repo.get_request(rid)
        start_place = data['start_point']
        start_time = data['start_time']
        max_seconds = data['max_seconds']
        places_data = data['filtered_places_data']
        exclude_low_rang_routes = data['exclude_low_rang_routes']
        root = data['root']

        places = self.__repo.get_places(places_data)
        routes = []
        is_limit = self.__build_tree(places, start_place, start_time, root['r'], 0, routes, [], max_seconds, True, exclude_low_rang_routes)
        if is_limit:
            if self.__repo.safe_root(rid, root):
                return routes
            else:
                return []
        self.__build_tree(places, start_place, start_time, root['l'], 0, routes, [], max_seconds, False, exclude_low_rang_routes)

        if self.__repo.safe_root(rid, root):
            return routes
        else:
            return []

    def get_recommended_route(
            self,
            token,
            user_id,
            included_categories,
            soft_excluded_categories,
            hard_excluded_categories,
            start_point,
            start_time,
            end_time,
            use_prev_history,
            use_common_weights,
            exclude_low_rang_routes
    ):
        error = self.__token_manager.verify_token(token, user_id)
        if error is not None:
            return None, None, error
        recommended_places, error = self.__get_recommended_places(
            user_id,
            included_categories,
            soft_excluded_categories,
            hard_excluded_categories,
            use_prev_history,
            use_common_weights
        )
        if error is not None:
            return None, None, error

        filtered_places, max_seconds, error = self.__filter_places(recommended_places, start_point, start_time, end_time)
        if error is not None:
            return None, None, error
        if not filtered_places or max_seconds <= 1800:
            return None, None, "По заданному промежутку времени и категориям невозможно сформировать маршрут"
        routes, root = self.__build_route(filtered_places, start_point, start_time, max_seconds, exclude_low_rang_routes)

        rid = self.__safe_request(
            user_id,
            included_categories,
            soft_excluded_categories,
            hard_excluded_categories,
            start_point,
            start_time,
            max_seconds,
            filtered_places,
            use_prev_history,
            use_common_weights,
            exclude_low_rang_routes,
            root
        )
        return routes, rid, None

    def __safe_request(
            self,
            user_id,
            included_categories,
            soft_excluded_categories,
            hard_excluded_categories,
            start_point,
            start_time,
            max_seconds,
            filtered_places,
            use_prev_history,
            use_common_weights,
            exclude_low_rang_routes,
            root
    ):
        filtered_places_data = []
        for place in filtered_places:
            filtered_places_data.append({
                'place_id': place['_id'],
                'rang': place['rang']
            })
        return self.__repo.safe_request(
            {
                'user_id': user_id,
                'included_categories': included_categories,
                'soft_excluded_categories': soft_excluded_categories,
                'hard_excluded_categories': hard_excluded_categories,
                'start_time': start_time,
                'start_point': start_point,
                'max_seconds': max_seconds,
                'filtered_places_data': filtered_places_data,
                'use_prev_history': use_prev_history,
                'use_common_weights': use_common_weights,
                'exclude_low_rang_routes': exclude_low_rang_routes,
                'root': root
            }
        )

    def __get_recommended_places(
            self,
            user_id,
            included_categories,
            soft_excluded_categories,
            hard_excluded_categories,
            use_prev_history,
            use_common_weights
    ):
        self.__update_vector(user_id, included_categories)

        if use_prev_history:
            user = self.__repo.get_user(user_id)
            user_vector = user['vector']
        else:
            categories_vector = self.__repo.get_all_categories_vector()
            for category in included_categories:
                categories_vector[category] = 1
            user_vector = categories_vector

        places = self.__repo.get_all_places()
        places_with_distance = []
        weights = self.__repo.get_categories_weight_vector(use_common_weights)

        for place in places:
            skip_place = False
            for category in hard_excluded_categories:
                if place['vector'][category] != 0:
                    skip_place = True
                    break
            if skip_place:
                continue

            copied_weights = weights.copy()
            for category in soft_excluded_categories:
                copied_weights[category] -= 0.1
            place_with_distance = place.copy()

            try:
                place_with_distance['rang'] = distance.cosine(
                    list(user_vector.values()),
                    list(place['vector'].values()),
                    list(copied_weights.values())
                )
            except:
                print("user_vector count: " + str(len(list(user_vector.values()))))
                print("place vector count: " + str(len(list(place['vector'].values()))))
                print("copied_weights count: " + str(len(list(copied_weights.values()))))
                place_with_distance['rang'] = 0

            places_with_distance.append(place_with_distance)

        return sorted(places_with_distance, key=lambda p: p['rang'], reverse=True), None

    def __filter_places(self, places, start_place, start_time, end_time):
        start_station = self.__repo.get_station(start_place)
        if start_station is None:
            return None, None, "Начальная точка введена неверно"
        durations = start_station['duration']

        start = start_time.split(':')
        end = end_time.split(':')
        minutes = 0
        hours = 0
        if int(end[1]) - int(start[1]) < 0:
            minutes = int(end[1]) + 60 - int(start[1])
            hours = int(end[0]) - int(start[0]) - 1
        else:
            minutes = int(end[1]) - int(start[1])
            hours = int(end[0]) - int(start[0])
        max_seconds = hours * 3600 + minutes * 60

        filtered_places = []
        for place in places:
            subways = place['subway'].split(', ')
            duration = -1
            for subway in subways:
                if subway in durations:
                    duration = durations[subway]
                    break
            if duration != -1 and duration <= max_seconds:
                filtered_places.append(place)

        return filtered_places, max_seconds, None

    def __update_vector(self, user_id, included_categories):
        user = self.__repo.get_user(user_id)
        user_vector = user['vector']
        categories_vector = self.__repo.get_all_categories_vector()
        for category in included_categories:
            categories_vector[category] = 1

        sum_vector = np.add.reduce([list(user_vector.values()), list(categories_vector.values())])
        normalized_vector = sum_vector / np.linalg.norm(sum_vector)

        categories_normalized_vector = dict()
        i = 0
        for category in categories_vector:
            categories_normalized_vector[category] = normalized_vector[i]
            i += 1
        self.__repo.update_user_vector(user_id, categories_normalized_vector)

    def __build_route(self, places, start_place, start_time, max_seconds, exclude_low_rang_routes):
        start_node_r = {
            'is_passed': False,
            'value': 0,
            'r': None,
            'l': None
        }
        start_node_l = {
            'is_passed': False,
            'value': 0,
            'r': None,
            'l': None
        }
        root = {
            'is_passed': False,
            'value': start_place,
            'r': start_node_r,
            'l': start_node_l
        }
        routes = []
        is_limit = self.__build_tree(places, start_place, start_time, start_node_r, 0, routes, [], max_seconds, True, exclude_low_rang_routes)
        if is_limit:
            return routes, root
        self.__build_tree(places, start_place, start_time, start_node_l, 0, routes, [], max_seconds, False, exclude_low_rang_routes)
        return routes, root

    def __build_tree(self, places, start_place, start_time, node, index, routes, cur_route, max_seconds, need_choose, exclude_low_rang_routes, limit=10):
        if node['is_passed']:
            return False
        if len(routes) == limit:
            return True
        last_place = index == len(places) - 1
        if index != len(places) - 1 and node['r'] is None and node['l'] is None:
            node['r'] = {
                'is_passed': False,
                'value': index + 1,
                'r': None,
                'l': None
            }
            node['l'] = {
                'is_passed': False,
                'value': index + 1,
                'r': None,
                'l': None
            }

        if need_choose:
            new_cur_route = cur_route + [places[node['value']]]
            if exclude_low_rang_routes and not self.__check_rang(new_cur_route):
                node['is_passed'] = True
                return False

            check_time, time_left = self.__check_time(start_place, new_cur_route, max_seconds)
            if check_time == 1:
                node['is_passed'] = True
            elif check_time == 2:
                optimized_route = self.__optimize_route(start_place, start_time, new_cur_route)
                routes.append(optimized_route)
                node['is_passed'] = True
                print("depth: " + str(index) + ", routes count: " + str(len(routes)))
            else:
                if not last_place:
                    is_limit = self.__build_tree(places, start_place, start_time, node['r'], index + 1, routes, new_cur_route, max_seconds, True, exclude_low_rang_routes)
                    if is_limit:
                        return True
                    is_limit = self.__build_tree(places, start_place, start_time, node['l'], index + 1, routes, new_cur_route, max_seconds, False, exclude_low_rang_routes)
                    if is_limit:
                        return True
                node['is_passed'] = True
        else:
            if not last_place:
                is_limit = self.__build_tree(places, start_place, start_time, node['r'], index + 1, routes, cur_route, max_seconds, True, exclude_low_rang_routes)
                if is_limit:
                    return True
                is_limit = self.__build_tree(places, start_place, start_time, node['l'], index + 1, routes, cur_route, max_seconds, False, exclude_low_rang_routes)
                if is_limit:
                    return True
            node['is_passed'] = True
        return False

    # 1 - route duration is more than max seconds
    # 2 - route is full
    # 3 - route is not full
    def __check_time(self, start_place, places, max_seconds, eps=2400):
        start_station = self.__repo.get_station(start_place)
        durations = start_station['duration']

        n = len(places)
        matrix = []
        start_row = [0]
        for place in places:
            subways = place['subway'].split(', ')
            duration = 0
            for subway in subways:
                if subway in durations:
                    duration = durations[subway]
                    break
            start_row.append(duration)
        matrix.append(start_row)

        for place in places:
            row = [0]
            for i in range(n):
                row.append(place['distance'][str(places[i]['_id'])])
            matrix.append(row)

        np_matrix = np.array(matrix)
        permutation, dist = solve_tsp_dynamic_programming(np_matrix)
        dist += 1800 * len(places)

        if dist > max_seconds:
            return 1, None
        elif max_seconds - dist <= eps:
            return 2, max_seconds - dist
        else:
            return 3, max_seconds - dist

    def __check_rang(self, places, limit=0):
        avg_rang = 0
        for place in places:
            avg_rang += place['rang']
        avg_rang /= len(places)
        return avg_rang >= limit

    def __optimize_route(self, start_place, start_time, route):
        start_station = self.__repo.get_station(start_place)
        durations = start_station['duration']

        n = len(route)
        matrix = []
        start_row = [0]
        for place in route:
            subways = place['subway'].split(', ')
            duration = 0
            for subway in subways:
                if subway in durations:
                    duration = durations[subway]
                    break
            start_row.append(duration)
        matrix.append(start_row)

        for place in route:
            row = [0]
            for i in range(n):
                row.append(place['distance'][str(route[i]['_id'])])
            matrix.append(row)

        np_matrix = np.array(matrix)
        permutation, dist = solve_tsp_dynamic_programming(np_matrix)

        start_time = start_time.split(":")
        next_place = route[permutation[1] - 1]
        sec_to_next_place = matrix[0][permutation[1]]
        hours_to_next_place = sec_to_next_place // 3600
        min_to_next_place = (sec_to_next_place % 3600) // 60
        cur_h = int(start_time[0]) + hours_to_next_place + (int(start_time[1]) + min_to_next_place) // 60
        cur_m = (int(start_time[1]) + min_to_next_place) % 60
        next_h = cur_h + (cur_m + 30) // 60
        next_m = (cur_m + 30) % 60
        need_zero = ""
        next_need_zero = ""
        if cur_m < 10:
            need_zero = "0"
        if next_m < 10:
            next_need_zero = "0"

        optimized_route = [
            {
                "step": start_place + " - " + next_place['title'],
                "time": start_time[0] + ":" + start_time[1] + " - " + str(cur_h) + ":" + need_zero + str(cur_m)
            },
            {
                "step": next_place['title'],
                "time": str(cur_h) + ":" + need_zero + str(cur_m) + " - " + str(next_h) + ":" + next_need_zero + str(next_m)
            }
        ]

        for i in range(len(permutation) - 1):
            if i != 0:
                cur_place = route[permutation[i] - 1]
                next_place = route[permutation[i + 1] - 1]

                sec_to_next_place = matrix[permutation[i]][permutation[i + 1]]
                hours_to_next_place = sec_to_next_place // 3600
                min_to_next_place = (sec_to_next_place - hours_to_next_place * 3600) // 60

                need_zero = ""
                if next_m < 10:
                    need_zero = "0"
                prev_time = str(next_h) + ":" + need_zero + str(next_m)

                cur_h = next_h + hours_to_next_place + (next_m + min_to_next_place) // 60
                cur_m = (next_m + min_to_next_place) % 60
                next_h = cur_h + (cur_m + 30) // 60
                next_m = (cur_m + 30) % 60
                need_zero = ""
                next_need_zero = ""
                if cur_m < 10:
                    need_zero = "0"
                if next_m < 10:
                    next_need_zero = "0"

                optimized_route += [
                    {
                        "step": cur_place['title'] + " - " + next_place['title'],
                        "time": prev_time + " - " + str(cur_h) + ":" + need_zero + str(cur_m)
                    },
                    {
                        "step": next_place['title'],
                        "time": str(cur_h) + ":" + need_zero + str(cur_m) + " - " + str(next_h) + ":" + next_need_zero + str(next_m)
                    }
                ]

        return optimized_route
