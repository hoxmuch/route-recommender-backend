from pymongo import MongoClient
from bson.objectid import ObjectId

import os
from dotenv import load_dotenv


class RouteRecommenderRepository:

    def __init__(self, config_file):
        try:
            dotenv_path = os.path.join(os.path.dirname(__file__), config_file)
            if os.path.exists(dotenv_path):
                load_dotenv(dotenv_path)

            host = os.environ.get('db_host')
            port = int(os.environ.get('db_port'))

            self.__client = MongoClient(host, port)
            self.__categories_vector, self.__slug_dict = self.__get_categories_vector()

        except Exception as e:
            print(e)

    def login(self, login, password):
        db = self.__client['route-recommender']
        user = db.users.find_one({
            'login': login,
            'password': password
        })
        if user is None:
            return None
        else:
            return str(user['_id'])

    def register(self, login, password):
        db = self.__client['route-recommender']
        if db.users.find_one({'login': login}) is not None:
            return None

        new_user = {
            'login': login,
            'password': password,
            'vector': self.__categories_vector
        }
        user_id = db.users.insert_one(new_user).inserted_id
        return str(user_id)

    def get_all_places(self):
        db = self.__client['route-recommender']
        return db.places.find()

    def get_places(self, places_data):
        db = self.__client['route-recommender']
        places = []
        for place_data in places_data:
            place = db.places.find_one({'_id': place_data['place_id']})
            place['rang'] = place_data['rang']
            places.append(place)
        return places

    def get_user(self, user_id):
        db = self.__client['route-recommender']
        user = db.users.find_one({
            '_id': ObjectId(user_id)
        })
        return user

    def get_story(self, user_id):
        db = self.__client['route-recommender']
        requests = db.requests.find({'user_id': user_id})
        story = []
        for request in requests:
            story.append({
                'included_categories': request['included_categories'],
                'soft_excluded_categories': request['soft_excluded_categories'],
                'hard_excluded_categories': request['hard_excluded_categories'],
                'start_point': request['start_point'],
                'use_prev_history': request['use_prev_history'],
                'use_common_weights': request['use_common_weights'],
                'exclude_low_rang_routes': request['exclude_low_rang_routes']
            })
        return story

    def update_user_vector(self, user_id, new_vector):
        db = self.__client['route-recommender']
        user = db.users.update_one(
            {'_id': ObjectId(user_id)},
            {'$set': {'vector': new_vector}}
        )

    def get_all_categories_vector(self):
        return self.__categories_vector.copy()

    def get_categories_weight_vector(self, use_common_weights):
        db = self.__client['route-recommender']
        weights = []
        if use_common_weights:
            weights = db.common_weights.find()
        else:
            weights = db.uncommon_weights.find()

        weights_dict = dict()
        for weight in weights:
            weights_dict[weight['category']] = weight['weight']
        return weights_dict

    def get_metro_stations(self):
        db = self.__client['route-recommender']
        return db.stations.find()

    def get_categories(self):
        db = self.__client['route-recommender']
        return db.categories.find()

    def get_station(self, name):
        db = self.__client['route-recommender']
        return db.stations.find_one({'name': name})

    def safe_request(self, data):
        db = self.__client['route-recommender']
        rid = db.requests.insert_one(data).inserted_id
        return rid

    def get_request(self, rid):
        db = self.__client['route-recommender']
        return db.requests.find_one({'_id': ObjectId(rid)})

    def safe_root(self, rid, root):
        db = self.__client['route-recommender']
        try:
            db.requests.update_one(
                {'_id': ObjectId(rid)},
                {'$set': {'root': root}}
            )
            return True
        except:
            return False

    def safe_feedback(self, rid, feedback):
        db = self.__client['route-recommender']
        db.feedbacks.insert_one(feedback)

    def __get_categories_vector(self):
        db = self.__client['route-recommender']
        categories = dict()
        slug_dict = dict()
        for category in db.categories.find():
            categories[category['name']] = 0
            if 'slug' in category:
                slug_dict[category['slug']] = category['name']
        return categories, slug_dict
