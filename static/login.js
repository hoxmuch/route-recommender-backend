const loginForm = document.getElementById("login-form");
const loginButton = document.getElementById("login-form-submit");
const loginErrorMsg = document.getElementById("login-error-msg");

const host = "84.252.139.52";
const port = "5010";

loginButton.addEventListener("click", (e) => {
    e.preventDefault();
    const username = loginForm.username.value;
    const password = loginForm.password.value;

    const request = new XMLHttpRequest();
    const url = "http://" + host + ":" + port + "/api/login?login=" + username + "&password=" + password;
    request.open("GET", url, true);
    request.responseType = "json";

    request.onload = function () {
        if (request.status !== 200) {
            console.log(request.response.error)
            loginErrorMsg.style.opacity = 1;
        } else {
            document.cookie = "jwt_token=" + request.response.token + "; path=/";
            document.cookie = "user_id=" + request.response.user_id + "; path=/";
            location.replace("http://" + host + ":" + port + "/route_builder");
        }
    }

    request.send();
})