const registerForm = document.getElementById("register-form");
const registerButton = document.getElementById("register-form-submit");
const registerErrorMsg = document.getElementById("register-error-msg");

const host = "84.252.139.52";
const port = "5010";

registerButton.addEventListener("click", (e) => {
    e.preventDefault();
    const username = registerForm.username.value;
    const password = registerForm.password.value;

    const request = new XMLHttpRequest();
    const url = "http://" + host + ":" + port + "/api/register" + "?login=" + username + "&password=" + password;
    request.open("POST", url, true);
    request.responseType = "json";

    request.onload = function () {
        if (request.status !== 200) {
            console.log(request.response.error)
            registerErrorMsg.style.opacity = 1;
        } else {
            document.cookie = "jwt_token=" + request.response.token + "; path=/";
            document.cookie = "user_id=" + request.response.user_id + "; path=/";
            location.replace("http://" + host + ":" + port + "/route_builder");
        }
    }

    request.send();
})