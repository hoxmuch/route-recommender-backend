const host = "84.252.139.52";
const port = "5010";

let includeCategoriesCheckList = document.getElementById('include-categories-list');
includeCategoriesCheckList.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (includeCategoriesCheckList.classList.contains('visible'))
    includeCategoriesCheckList.classList.remove('visible');
  else
    includeCategoriesCheckList.classList.add('visible');
}

let hardExcludeCategoriesCheckList = document.getElementById('hard-exclude-categories-list');
hardExcludeCategoriesCheckList.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (hardExcludeCategoriesCheckList.classList.contains('visible'))
    hardExcludeCategoriesCheckList.classList.remove('visible');
  else
    hardExcludeCategoriesCheckList.classList.add('visible');
}

let softExcludeCategoriesCheckList = document.getElementById('soft-exclude-categories-list');
softExcludeCategoriesCheckList.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (softExcludeCategoriesCheckList.classList.contains('visible'))
    softExcludeCategoriesCheckList.classList.remove('visible');
  else
    softExcludeCategoriesCheckList.classList.add('visible');
}

let startHourSelect = document.querySelector('#start-hour');
let startMinuteSelect = document.querySelector('#start-minute');
let endHourSelect = document.querySelector('#end-hour');
let endMinuteSelect = document.querySelector('#end-minute');
let metroStationsDatalist = document.getElementById('metro-station-name')

function populateStartHours() {
  for(let i = 8; i <= 22; i++) {
    let option = document.createElement('option');
    option.textContent = i;
    startHourSelect.appendChild(option);
  }
}

function populateStartMinutes() {
  for(let i = 0; i <= 59; i++) {
    let option = document.createElement('option');
    option.textContent = (i < 10) ? ("0" + i) : i;
    startMinuteSelect.appendChild(option);
  }
}

function populateEndHours() {
  for(let i = 8; i <= 22; i++) {
    let option = document.createElement('option');
    option.textContent = i;
    endHourSelect.appendChild(option);
  }
}

function populateEndMinutes() {
  for(let i = 0; i <= 59; i++) {
    let option = document.createElement('option');
    option.textContent = (i < 10) ? ("0" + i) : i;
    endMinuteSelect.appendChild(option);
  }
}

function loadMetroStations() {
  const request = new XMLHttpRequest();
  const url = "http://" + host + ":" + port + "/api/metro";
  request.open("GET", url, true);
  request.responseType = "json";

  request.onload = function () {
    if (request.status !== 200) {
      console.log("Some error while getting metro stations")
      console.log(request.response)
    } else {
      let metroStations = request.response.metro_stations
      metroStations.forEach(function (station) {
        let option = document.createElement('option');
        option.textContent = station;
        metroStationsDatalist.appendChild(option);
      });
    }
  }

  request.send();
}

function loadCategories() {
  const request = new XMLHttpRequest();
  const url = "http://" + host + ":" + port + "/api/categories";
  request.open("GET", url, true);
  request.responseType = "json";

  request.onload = function () {
    if (request.status !== 200) {
      console.log("Some error while getting metro stations");
      console.log(request.response);
    } else {
      let categories = request.response.categories
      categories.forEach(function (category) {
        let items = [includeItems, softExcludeItems, hardExcludeItems];
        for (let i = 0; i < 3; i++) {
          let li = document.createElement('li');
          let inputCheckbox = document.createElement('input');
          inputCheckbox.type = "checkbox";

          li.appendChild(inputCheckbox);
          li.appendChild(document.createTextNode(category));
          items[i].appendChild(li);
        }
      });
    }
  }

  request.send();
}

loadCategories();
populateStartHours();
populateStartMinutes();
populateEndHours();
populateEndMinutes();
loadMetroStations();

const getCookie = (name) => {
  return document.cookie.split('; ').reduce((r, v) => {
    const parts = v.split('=')
    return parts[0] === name ? decodeURIComponent(parts[1]) : r
  }, '')
}


const includeItems = document.getElementById("include-items");
const softExcludeItems = document.getElementById("soft-exclude-items");
const hardExcludeItems = document.getElementById("hard-exclude-items");

const usePrevHistoryInput = document.getElementById("use-prev-history");
const useUncommonWeightsInput = document.getElementById("use-uncommon-weights");
const excludeLowRangRoutesInput = document.getElementById("exclude-low-rang-routes");

const metroStationInput = document.getElementById("metro-station-input");

const showStoryButton = document.getElementById("show-story-submit");
const buildRouteButton = document.getElementById("build-route-submit");

const routeTableList = document.getElementById("route-table-list");
const nextRouteBatchDiv = document.getElementById("next-route-batch");
let tableIndex = 0;
let routes = [];

function buildParams() {
  let params = "";

  let included_categories = "";
  let included = includeItems.getElementsByTagName("li");
  for (let i = 0; i < included.length; ++i) {
    if (included[i].firstChild.checked) {
      included_categories += included[i].lastChild.textContent;
      included_categories += ",";
    }
  }
  included_categories = included_categories.slice(0, -1);

  let soft_excluded_categories = "";
  let softExcluded = softExcludeItems.getElementsByTagName("li");
  for (let i = 0; i < softExcluded.length; ++i) {
    if (softExcluded[i].firstChild.checked) {
      soft_excluded_categories += softExcluded[i].lastChild.textContent;
      soft_excluded_categories += ",";
    }
  }
  soft_excluded_categories = soft_excluded_categories.slice(0, -1);

  let hard_excluded_categories = "";
  let hardExcluded = hardExcludeItems.getElementsByTagName("li");
  for (let i = 0; i < hardExcluded.length; ++i) {
    if (hardExcluded[i].firstChild.checked) {
      hard_excluded_categories += hardExcluded[i].lastChild.textContent;
      hard_excluded_categories += ",";
    }
  }
  hard_excluded_categories = hard_excluded_categories.slice(0, -1);

  const start_point = metroStationInput.value;
  const start_time = startHourSelect.value + ":" + startMinuteSelect.value;
  const end_time = endHourSelect.value + ":" + endMinuteSelect.value;
  const use_prev_history = usePrevHistoryInput.checked ? 1 : 0;
  const use_common_weights = useUncommonWeightsInput.checked ? 0 : 1;
  const exclude_low_rang_routes = excludeLowRangRoutesInput.checked ? 1 : 0;

  params +=
      "included_categories=" + included_categories +
      "&soft_excluded_categories=" + soft_excluded_categories +
      "&hard_excluded_categories=" + hard_excluded_categories +
      "&start_point=" + start_point +
      "&start_time=" + start_time +
      "&end_time=" + end_time +
      "&use_prev_history=" + use_prev_history +
      "&use_common_weights=" + use_common_weights +
      "&exclude_low_rang_routes=" + exclude_low_rang_routes;

  return params
}

function buildDropDownSelect(list, parent, id) {
  let selectList = document.createElement("select");
  selectList.id = id;
  parent.appendChild(selectList);

  for (let i = 0; i < list.length; i++) {
    let option = document.createElement("option");
    option.value = list[i];
    option.text = list[i];
    selectList.appendChild(option);
  }
}

buildRouteButton.addEventListener("click", (e) => {
    e.preventDefault();
    clearTableList();

    const request = new XMLHttpRequest();
    let user_id = getCookie("user_id");
    let token = getCookie("jwt_token");
    let params = buildParams();
    console.log(params);
    const url = "http://" + host + ":" + port + "/api/user/" + user_id + "/route/recommend?" + params;

    request.responseType = "json";
    request.open("GET", url, true);
    request.setRequestHeader("Authorization", "Bearer " + token);

    request.onload = function () {
        if (request.status !== 200) {
            alert(request.response.error)
        } else {
          document.cookie = "rid=" + request.response.rid + "; path=/";

          let routesList = request.response.routes;
          routes = routesList;
          routesList.forEach(function (route) {
            let tableDiv = document.createElement("div");
            tableDiv.id = "route-" + tableIndex;
            tableDiv.className = "some-block";
            let tbl = document.createElement("table");
            let tblBody = document.createElement("tbody");

            let stepIndex = 1;
            route.forEach(function (route_step) {
              let step = route_step.step;
              let time = route_step.time;
              let row = document.createElement("tr");

              let cellRowIndex = document.createElement("td");
              let cellRowIndexText = document.createTextNode(stepIndex);
              cellRowIndex.appendChild(cellRowIndexText);
              row.appendChild(cellRowIndexText);

              let cellStep = document.createElement("td");
              let cellStepText = document.createTextNode(step);
              cellStep.appendChild(cellStepText);
              row.appendChild(cellStep);

              let cellTime = document.createElement("td");
              let cellTimeText = document.createTextNode(time);
              cellTime.appendChild(cellTimeText);
              row.appendChild(cellTime);

              tblBody.appendChild(row);
              stepIndex += 1;
            });
            tbl.appendChild(tblBody);
            tbl.setAttribute("border", "2");
            tableDiv.appendChild(tbl);

            let userTypeDiv = document.createElement("div");
            userTypeDiv.id = "user-type-div-" + tableIndex;
            userTypeDiv.className = "some-block";
            tableDiv.appendChild(document.createTextNode("Пожалуйста, выберите тип пользователя, которому вы соответствуете больше всего"));
            tableDiv.appendChild(userTypeDiv);
            buildDropDownSelect(
                ["Я опытный пользователь, знающий много достопримечательностей и интересных мест в Москве",
                  "Я пользователь с небольшим опытом, знаю мало доостопримечательностей и интересных мест в Москве"],
                userTypeDiv,
                "user-type-" + tableIndex
            );

            let overallDiv = document.createElement("div");
            overallDiv.id = "overall-div-" + tableIndex;
            overallDiv.className = "some-block";
            tableDiv.appendChild(document.createTextNode("Пожалуйста, выберите общую оценку маршрута"));
            tableDiv.appendChild(overallDiv);
            buildDropDownSelect(["Положительное", "Нейтральное", "Отрицательное"], overallDiv, "overall-" + tableIndex);

            let generalDiv = document.createElement("div");
            generalDiv.id = "general-div-" + tableIndex;
            generalDiv.className = "some-block";
            tableDiv.appendChild(document.createTextNode("Пожалуйста, оцените сам маршрут и на сколько он соответствует вашим ожиданиям"));
            tableDiv.appendChild(generalDiv);
            buildDropDownSelect(
                ["Соответствует ожиданиям, хорошо", "Соответствует ожиданиям, плохо",
                  "Не соответствует ожиданиям, хорошо", "Не соответствует ожиданиям, плохо"],
                generalDiv,
                "general-" + tableIndex
            );

            let someNewCheckboxDiv = document.createElement("div");
            someNewCheckboxDiv.id = "some-new-div-" + tableIndex;
            someNewCheckboxDiv.className = "some-block";
            let someNewCheckbox = document.createElement('input');
            someNewCheckbox.type = "checkbox";
            someNewCheckbox.id = "some-new-" + tableIndex;
            someNewCheckboxDiv.appendChild(someNewCheckbox);
            someNewCheckboxDiv.appendChild(document.createTextNode("Я нашел для себя что-то новое"));
            tableDiv.appendChild(someNewCheckboxDiv);

            let seenBeforeCheckboxDiv = document.createElement("div");
            seenBeforeCheckboxDiv.id = "seen-before-div-" + tableIndex;
            seenBeforeCheckboxDiv.className = "some-block";
            let seenBeforeCheckbox = document.createElement('input');
            seenBeforeCheckbox.type = "checkbox";
            seenBeforeCheckbox.id = "seen-before-" + tableIndex;
            seenBeforeCheckboxDiv.appendChild(seenBeforeCheckbox);
            seenBeforeCheckboxDiv.appendChild(document.createTextNode("Я там уже был"));
            tableDiv.appendChild(seenBeforeCheckboxDiv)

            let feedbackTextareaDiv = document.createElement("div");
            feedbackTextareaDiv.id = "feedback-div-" + tableIndex;
            feedbackTextareaDiv.className = "some-block";
            let feedbackTextarea = document.createElement("textarea");
            feedbackTextarea.id = "feedback-" + tableIndex;
            feedbackTextarea.cols = 200;
            feedbackTextarea.rows = 5;
            feedbackTextareaDiv.appendChild(document.createTextNode("Пожалуйста, оставьте отзыв о маршруте:"));
            feedbackTextareaDiv.appendChild(feedbackTextarea);
            tableDiv.appendChild(feedbackTextareaDiv);

            let sendFeedbackSubmit = document.createElement("input");
            sendFeedbackSubmit.type = "submit";
            sendFeedbackSubmit.id = "send-feedback-" + tableIndex;
            sendFeedbackSubmit.value = "Отправить"
            sendFeedbackSubmit.addEventListener("click", sendFeedback);
            tableDiv.appendChild(sendFeedbackSubmit);

            routeTableList.appendChild(tableDiv);
            tableIndex += 1;
          });

          const nextRouteBatchSubmit = document.createElement("input")
          nextRouteBatchSubmit.type = "submit";
          nextRouteBatchSubmit.value = "Далее";
          nextRouteBatchSubmit.addEventListener("click", nextRouteBatch);
          nextRouteBatchDiv.appendChild(nextRouteBatchSubmit);
        }
    }
    request.send();
})

function sendFeedback(e) {
  e.preventDefault();

  const parent = this.parentElement;
  const index = Number(parent.id.split('-')[1]);

  const request = new XMLHttpRequest();
  let user_id = getCookie("user_id");
  let rid = getCookie("rid");
  const url = "http://" + host + ":" + port + "/api/user/" + user_id + "/route/feedback?rid=" + rid;

  const feedbackData = {
    "feedback": {
      "user_id": user_id,
      "rid": rid,
      "index": index,
      "route": routes[index],
      "user_type": document.getElementById("user-type-" + index).value,
      "overall": document.getElementById("overall-" + index).value,
      "general": document.getElementById("general-" + index).value,
      "some_new": document.getElementById("some-new-" + index).checked,
      "seen_before": document.getElementById("seen-before-" + index).checked,
      "feedback": document.getElementById("feedback-" + index).value
    }
  };

  request.open("POST", url, true);
  request.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  request.send(JSON.stringify(feedbackData));

  alert("Отзыв отправлен");
}

function nextRouteBatch(e) {
  e.preventDefault();

  const request = new XMLHttpRequest();
  let user_id = getCookie("user_id");
  let rid = getCookie("rid");
  const url = "http://" + host + ":" + port + "/api/user/" + user_id + "/route/next?rid=" + rid;

  request.open("GET", url, true);
  request.responseType = "json";

  request.onload = function () {
    if (request.status !== 200) {
      alert(request.response.error)
    } else {
      let routesList = request.response.routes;
      routes.push(routesList);
      if (routesList.length == 0) {
        while (nextRouteBatchDiv.firstChild) {
          nextRouteBatchDiv.removeChild(nextRouteBatchDiv.firstChild);
        }
        return
      }
      routesList.forEach(function (route) {
        let tableDiv = document.createElement("div");
        tableDiv.id = "route-" + tableIndex;
        tableDiv.className = "some-block";
        let tbl = document.createElement("table");
        let tblBody = document.createElement("tbody");

        let stepIndex = 1;
        route.forEach(function (route_step) {
          let step = route_step.step;
          let time = route_step.time;
          let row = document.createElement("tr");

          let cellRowIndex = document.createElement("td");
          let cellRowIndexText = document.createTextNode(stepIndex);
          cellRowIndex.appendChild(cellRowIndexText);
          row.appendChild(cellRowIndexText);

          let cellStep = document.createElement("td");
          let cellStepText = document.createTextNode(step);
          cellStep.appendChild(cellStepText);
          row.appendChild(cellStep);

          let cellTime = document.createElement("td");
          let cellTimeText = document.createTextNode(time);
          cellTime.appendChild(cellTimeText);
          row.appendChild(cellTime);

          tblBody.appendChild(row);
          stepIndex += 1;
        });
        tbl.appendChild(tblBody);
        tbl.setAttribute("border", "2");
        tableDiv.appendChild(tbl);

        let userTypeDiv = document.createElement("div");
        userTypeDiv.id = "user-type-div-" + tableIndex;
        userTypeDiv.className = "some-block";
        tableDiv.appendChild(document.createTextNode("Пожалуйста, выберите тип пользователя, которому вы соответствуете больше всего"));
        tableDiv.appendChild(userTypeDiv);
        buildDropDownSelect(
            ["Я опытный пользователь, знающий много достопримечательностей и интересных мест в Москве",
              "Я пользователь с небольшим опытом, знаю мало доостопримечательностей и интересных мест в Москве"],
            userTypeDiv,
            "user-type-" + tableIndex
        );

        let overallDiv = document.createElement("div");
        overallDiv.id = "overall-div-" + tableIndex;
        overallDiv.className = "some-block";
        tableDiv.appendChild(document.createTextNode("Пожалуйста, выберите общую оценку маршрута"));
        tableDiv.appendChild(overallDiv);
        buildDropDownSelect(["Положительное", "Нейтральное", "Отрицательное"], overallDiv, "overall-" + tableIndex);

        let generalDiv = document.createElement("div");
        generalDiv.id = "general-div-" + tableIndex;
        generalDiv.className = "some-block";
        tableDiv.appendChild(document.createTextNode("Пожалуйста, оцените сам маршрут и на сколько он соответствует вашим ожиданиям"));
        tableDiv.appendChild(generalDiv);
        buildDropDownSelect(
            ["Соответствует ожиданиям, хорошо", "Соответствует ожиданиям, плохо",
              "Не соответствует ожиданиям, хорошо", "Не соответствует ожиданиям, плохо"],
            generalDiv,
            "general-" + tableIndex
        );

        let someNewCheckboxDiv = document.createElement("div");
        someNewCheckboxDiv.id = "some-new-div-" + tableIndex;
        someNewCheckboxDiv.className = "some-block";
        let someNewCheckbox = document.createElement('input');
        someNewCheckbox.type = "checkbox";
        someNewCheckbox.id = "some-new-" + tableIndex;
        someNewCheckboxDiv.appendChild(someNewCheckbox);
        someNewCheckboxDiv.appendChild(document.createTextNode("Я нашел для себя что-то новое"));
        tableDiv.appendChild(someNewCheckboxDiv);

        let seenBeforeCheckboxDiv = document.createElement("div");
        seenBeforeCheckboxDiv.id = "seen-before-div-" + tableIndex;
        seenBeforeCheckboxDiv.className = "some-block";
        let seenBeforeCheckbox = document.createElement('input');
        seenBeforeCheckbox.type = "checkbox";
        seenBeforeCheckbox.id = "seen-before-" + tableIndex;
        seenBeforeCheckboxDiv.appendChild(seenBeforeCheckbox);
        seenBeforeCheckboxDiv.appendChild(document.createTextNode("Я там уже был"));
        tableDiv.appendChild(seenBeforeCheckboxDiv)

        let feedbackTextareaDiv = document.createElement("div");
        feedbackTextareaDiv.id = "feedback-div-" + tableIndex;
        feedbackTextareaDiv.className = "some-block";
        let feedbackTextarea = document.createElement("textarea");
        feedbackTextarea.id = "feedback-" + tableIndex;
        feedbackTextarea.cols = 200;
        feedbackTextarea.rows = 5;
        feedbackTextareaDiv.appendChild(document.createTextNode("Пожалуйста, оставьте отзыв о маршруте:"));
        feedbackTextareaDiv.appendChild(feedbackTextarea);
        tableDiv.appendChild(feedbackTextareaDiv);

        let sendFeedbackSubmit = document.createElement("input");
        sendFeedbackSubmit.type = "submit";
        sendFeedbackSubmit.id = "send-feedback-" + tableIndex;
        sendFeedbackSubmit.value = "Отправить"
        sendFeedbackSubmit.addEventListener("click", sendFeedback);
        tableDiv.appendChild(sendFeedbackSubmit);

        routeTableList.appendChild(tableDiv);
        tableIndex += 1;
      });
    }
  }
  request.send();
}

function clearTableList() {
  while (nextRouteBatchDiv.firstChild) {
    nextRouteBatchDiv.removeChild(nextRouteBatchDiv.firstChild);
  }

  while (routeTableList.firstChild) {
    routeTableList.removeChild(routeTableList.firstChild);
  }
  tableIndex = 0;
}

showStoryButton.addEventListener("click", () => {
  const url = "http://" + host + ":" + port + "/story";
  window.open(url, '_blank').focus();
})